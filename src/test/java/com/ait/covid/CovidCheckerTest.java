package com.ait.covid;

import static org.junit.jupiter.api.Assertions.*;

import java.util.stream.Stream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

class CovidCheckerTest {

    CovidChecker checker = new CovidChecker();

    @BeforeEach
    void init() {
        checker = new CovidChecker();
    }

    @ParameterizedTest
    @ValueSource(doubles = { 29.9, 29.8, 50.01, 50.02 })
    void testTemperatureIsOutOfRange(double temp) {
        assertThrows(IllegalArgumentException.class, () -> {
            checker.checkTempForCovid(temp);
        });
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/com/ait/covid/temp-data.csv", delimiter = ',')
    void testValidTemperatures(String expected, double value) {
        assertThat(checker.checkTempForCovid(value), is(expected));
    }

    @ParameterizedTest
    @MethodSource("BPInvalidInputFactory")
    void testInvalidNumberOfBPInputs(int[] values) throws Exception {
        assertThrows(IllegalArgumentException.class, () -> {
            checker.checkBPForCovid(values);
        });
    }

    @ParameterizedTest
    @MethodSource("BPValidInputFactory")
    void testValidBPInputs(int[] values, String expected) {
        assertThat(checker.checkBPForCovid(values), is(expected));
    }

    static Stream<Arguments> BPInvalidInputFactory() {
        return Stream.of(Arguments.of(new int[] { 65, 65, 65, 65 }), Arguments.of(new int[] { 65, 65, 65, 65, 65, 65 }),
                Arguments.of(new int[] { 58, 65, 65, 65, 65 }), Arguments.of(new int[] { 65, 65, 59, 65, 65 }),
                Arguments.of(new int[] { 65, 91, 65, 65, 65 }), Arguments.of(new int[] { 65, 65, 65, 92, 65 }));
    }

    static Stream<Arguments> BPValidInputFactory(){
	       int[] avg73 = new int[] {70,72,73,74,76};
	       int[] avg74 = new int[] {74,74,74,74,74};
	       int[] avg75 = new int[] {70,73,75,77,80};
	       int[] avg60 = new int[] {60,60,60,60,60};
	       int[] avg90 = new int[] {90,90,90,90,90};
	       
	        return Stream.of(
	                Arguments.of(avg73, "OK"),
	                Arguments.of(avg74, "OK"),
	                Arguments.of(avg75, "TEST FOR COVID"),
	                Arguments.of(avg60, "OK"),
	                Arguments.of(avg90, "TEST FOR COVID")
	                );
	    }

}
