package com.ait.retailers;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.hamcrest.Matchers.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.Times;

class BiggestBazarRetailTest {

	@Mock
	Inventory inventory;
	@Mock
	PublicAddressSystem publicAddressSystem;
	BiggestBazarRetail biggestBazarRetail;

	@BeforeEach
	void init() {
		MockitoAnnotations.initMocks(this);
		biggestBazarRetail = new BiggestBazarRetail(inventory, publicAddressSystem);
	}

	@ParameterizedTest
	@MethodSource(value = "discountExpireIn30DaysFactory")
	void issueDiscountExpireIn30DaysTest(ArrayList<Item> arr, double discount, int expectedUpdate, int expected) {
		when(inventory.getItemsExpireInAMonth()).thenReturn(arr);
		when(inventory.itemsUpdated()).thenReturn(expectedUpdate);
		assertThat(biggestBazarRetail.issueDiscountForItemsExpireIn30Days(discount), is(expected));
		verify(publicAddressSystem, new Times(expectedUpdate)).announce(Mockito.isA(Offer.class));
		verify(inventory, new Times(expectedUpdate)).update(Mockito.isA(Item.class), Mockito.anyDouble());

	}

	static Stream<Arguments> discountExpireIn30DaysFactory() {
		
		Arguments qualifyForDiscount = ShopArugments.of(
				new ArrayList<>(
						List.of(
								new Item("1111", "Biscuits", 3d, 2d))
						)
				, 0.33
				, 1
				, 1);
		Arguments DisqualifiedForDiscount = ShopArugments.of(
				new ArrayList<>(
						List.of(
								new Item("1111", "Biscuits", 3d, 2d))
						)
				, 0.34
				, 0
				, 0);
		Arguments twoQualifiedForDiscount = ShopArugments.of(
				new ArrayList<>(
						List.of(
								new Item("1111", "Biscuits", 3d, 2d),
								new Item("1112", "Bread", 3.33, 2.22),
								new Item("1113", "Unreal Bread", 3.33, 3d))
						)
				, 0.33
				, 2
				, 2);
		
		return Stream.of(qualifyForDiscount, DisqualifiedForDiscount, twoQualifiedForDiscount);
	}

}

interface ShopArugments extends Arguments {
	static Arguments of(ArrayList<Item> items, double discount, int expectedUpdate, int expected) {
		return Arguments.of(items, discount, expectedUpdate, expected);
	}

}
