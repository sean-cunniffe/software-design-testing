package com.ait.example_assessment;

import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.hamcrest.Matchers.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

class CovidCheckerTest {

    CovidChecker checker;

    @BeforeEach
    void setUp() throws Exception {
        checker = new CovidChecker();
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/com/ait/example_assessment/temp-data.csv")
    void testTemperatureIsInRange(double temp, String expected) throws Exception {
        String value = checker.checkTempForCovid(temp);
        assertThat(value, is(expected));
    }

    @ParameterizedTest
    @ValueSource(doubles = { 29d, 51d })
    void testTemperatureIsOutOfRange(double temp) {
        assertThrows(IllegalArgumentException.class, () -> {
            checker.checkTempForCovid(temp);
        });
    }

    @ParameterizedTest
    @MethodSource(value = "bloodPressureValidFactory")
    void testBPIsInRange(int[] bp, String expected) {
        assertThat(checker.checkBPForCovid(bp), is(expected));
    }

    static Object[][] bloodPressureValidFactory() {
        int[] test1 = new int[] { 60, 61, 74, 75, 76 };
        int[] test2 = new int[] { 65, 66, 67, 68, 69 };
        int[] test3 = new int[] { 90, 89, 88, 87, 86 };
        return new Object[][] { { test1, "OK" }, { test2, "OK" }, { test3, "TEST FOR COVID" } };
    }

    @ParameterizedTest
    @MethodSource(value = "bloodPressureInValidFactory")
    void testBPIsOutRange(int[] bp) {
        assertThrows(IllegalArgumentException.class, () -> {
            checker.checkBPForCovid(bp);
        });
    }

    static int[][] bloodPressureInValidFactory() {
        int[] test1 = new int[] { 60, 61, 74, 91, 76 };
        int[] test2 = new int[] { 65, 66, 59, 90, 69 };
        int[] test3 = new int[] { 90, 91, 59, 87, 86 };
        return new int[][] { test1, test2, test3 };
    }

}
