package com.ait.leapyear;

import static org.junit.jupiter.api.Assertions.*;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.hamcrest.MatcherAssert.assertThat; 
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class JavaUtilTest {

	@Test
	void testNormalLeapYearIsLeap() {
		assertTrue(JavaUtil.isLeapYear(1992));
	}
	
	@Test
	void testNormalLeapYearIsLeapExample2() {
		assertTrue(JavaUtil.isLeapYear(1996));
	}
	
	@Test
	void non1LeapYearIsNotLeap() {
		assertFalse(JavaUtil.isLeapYear(1991));
	}
	
	@Test
	void centuryYearsAreNotLeap() {
		assertFalse(JavaUtil.isLeapYear(1900));
	}
	
	@Test
	void year2000WasLeap() {
		assertTrue(JavaUtil.isLeapYear(2000));
	}
	
	@ParameterizedTest
	@CsvSource(value= {"1992:true","1996:true", "1991:false", "2000:true", "1990:false"}, delimiter = ':')
	void isleapYear(int year, boolean shouldBeLeap){
//	    assertEquals(shouldBeLeap ,JavaUtil.isLeapYear(year));
	    assertThat(JavaUtil.isLeapYear(year), is(shouldBeLeap));
	}

}
