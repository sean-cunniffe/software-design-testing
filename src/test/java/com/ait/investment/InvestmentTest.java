package com.ait.investment;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

class InvestmentTest {

    @BeforeEach
    void setUp() throws Exception {
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/com/ait/investment/investment.csv", delimiter = ',')
    void testAllowedValues(int term, int startAmount, double result) {
        assertEquals(result, Investment.calculateInvestmentValue(term, startAmount));
    }

    @ParameterizedTest
    @CsvFileSource(resources = "/com/ait/investment/investment_exception.csv", delimiter = ',')
    void testOverAndUnderInvestmentAndTermRanges(int term, int startAmount) {
        assertThrows(IllegalArgumentException.class,() -> {
            Investment.calculateInvestmentValue(term, startAmount);
        });
    }

}
