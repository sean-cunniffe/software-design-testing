package com.ait.stock;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.verification.Times;

class StockListenerTest {

    StockListener listener;
    @Mock
    StockBroker broker;
    
    @BeforeEach
    void setUp() throws Exception {
//        broker = mock(StockBroker.class);
    	MockitoAnnotations.initMocks(broker);
        listener = new StockListener(broker);
    }

    @ParameterizedTest
    @MethodSource(value = "stockPriceFactory")
    void testValue(double boughtValue, double currentValue,int buyTimes, int sellTimes) {
        Stock stock = new Stock("FB", boughtValue);
        when(broker.getQoute(stock)).thenReturn(currentValue);
        listener.takeAction(stock);
        // by using anyInt, if the number of buys or sells change after refactoring code, the test still work
        verify(broker, new Times(buyTimes)).buy(Mockito.eq(stock), Mockito.anyInt());
        verify(broker, new Times(sellTimes)).sell(Mockito.eq(stock), Mockito.anyInt());
    }
    
    public static Stream<Arguments> stockPriceFactory() {
        return Stream.of(
                Arguments.of(25.0,26.0,0,1),
                Arguments.of(26.0,26.0,1,0),
                Arguments.of(26.0,25.0,1,0));
    }

}
