package com.ait.test_selenium;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class SalesTax {

    WebDriver driver;
    
    @BeforeEach
    void init() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\seanc\\chrome driver\\chromedriver.exe");
        driver= new ChromeDriver();
        driver.get("C:\\Users\\seanc\\Downloads\\sales_tax\\sales_tax\\sales_tax.html");
    }
    
    @AfterEach
    void exit() {
        driver.quit();
    }
    
    @Test
    void testTitle() throws Exception {
        assertEquals("Sales Tax Calculator", driver.getTitle());
    }
    
    @Test
    void testCalculateTotal() throws Exception {
        WebElement element = driver.findElement(By.id("subtotal"));
        element.clear();
        element.sendKeys("100");
        
        element = driver.findElement(By.id("taxRate"));
        element.clear();
        element.sendKeys("10");
        
        element = driver.findElement(By.id("calculate"));
        element.click();
        
        element = driver.findElement(By.id("salesTax"));
        assertEquals("10", element.getAttribute("value"));
        
        element = driver.findElement(By.id("total"));
        assertEquals("110.00", element.getAttribute("value"));
    }

}
