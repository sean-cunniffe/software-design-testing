package com.ait.game;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GameTest {

	Game game;

	@BeforeEach
	void init() throws Exception {
		game = new Game();
	}

	@Test
	void testGameRuns() throws Exception {
		// set user input
	    String steps = "k\nj\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny";
		InputStream stream = new ByteArrayInputStream(steps.getBytes());
		// set System.in as stream we defined
		System.setIn(stream);
		assertDoesNotThrow(() -> {
			game.start();
		});
	}
	
	@Test
	void testGameStopped() throws Exception {
		String steps = "k\nj\ny\nn";
		InputStream stream = new ByteArrayInputStream(steps.getBytes());
		System.setIn(stream);
		assertDoesNotThrow(() -> {
			game.start();
		});
	}
}
