package com.ait.game;

import static org.hamcrest.MatcherAssert.assertThat; 
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;

class DieTest {
	
	Die die;
	
	@BeforeEach
	void init() {
		die = new Die();
	}
	
	@RepeatedTest(20)
	void testRollGreaterThan0AndLessThanOrEqual6() throws Exception {
		die.roll();
		assertThat(die.getValue(),greaterThanOrEqualTo(1));
		assertThat(die.getValue(),lessThanOrEqualTo(6));
	}
	
	@Test
	void testGetValue() {
		assertEquals(1, die.getValue());
	}

}
