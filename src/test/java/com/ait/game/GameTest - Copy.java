package com.ait.game;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class GameTestCopy {

	Game game;

	@BeforeEach
	void init() throws Exception {
		game = new Game();
	}

	@Test
	void testGameRuns() throws Exception {
		// set user input
	    String steps = "k\nj\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny";
		InputStream stream = new ByteArrayInputStream(steps.getBytes());
		// set System.in as stream we defined
		System.setIn(stream);
		assertDoesNotThrow(() -> {
			game.start();
		});
	}
	
	@Test
	void testGameRunsWithDraw() throws Exception {
		PairOfDice dice = Mockito.mock(PairOfDice.class);
		doReturn(6).when(dice).getValue1();
		doReturn(6).when(dice).getValue2();
		doReturn(12).when(dice).getSum();
		game.setPairOfDice(dice);
		String steps = "k\nj\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny";
		InputStream stream = new ByteArrayInputStream(steps.getBytes());
		System.setIn(stream);
		assertDoesNotThrow(() -> {
			game.start();
		});
	}
	
	@Test
	void testPlayer2Wins() {
		// spy player1
		Player player1 = new Player("k");
		Player player1Spy = Mockito.spy(player1);
		doNothing().when(player1Spy).setTotalScore(Mockito.anyInt());
		// spy game
		Game gameSpy = Mockito.spy(game);
		doReturn(player1Spy).when(gameSpy).getPlayer1();
		
		String steps = "k\nj\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny";
		InputStream stream = new ByteArrayInputStream(steps.getBytes());
		System.setIn(stream);
		assertDoesNotThrow(() -> {
			gameSpy.start();
		});
	}
	
	@Test
	void testPlayer1Wins() {
		// spy player1
		Player player2 = new Player("j");
		Player player2Spy = Mockito.spy(player2);
		doNothing().when(player2Spy).setTotalScore(Mockito.anyInt());
		// spy game
		Game gameSpy = Mockito.spy(game);
		doReturn(player2Spy).when(gameSpy).getPlayer2();
		
		String steps = "k\nj\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny\ny";
		InputStream stream = new ByteArrayInputStream(steps.getBytes());
		System.setIn(stream);
		assertDoesNotThrow(() -> {
			gameSpy.start();
		});
	}
}
