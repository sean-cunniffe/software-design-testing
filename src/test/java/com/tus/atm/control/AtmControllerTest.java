package com.tus.atm.control;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.internal.verification.Times;

import static org.hamcrest.core.Is.*;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.tus.atm.io.AtmInterface;
import com.tus.boundaries.AccountDAO;
import com.tus.boundaries.NotificationService;
import com.tus.exception.ATMAccountException;
import com.tus.exception.ATMInsufficientFundsException;
import com.tus.exception.ATMPINLimitException;

class AtmControllerTest {

    AtmController atmController;
    AccountDAO accountDAO;
    AtmInterface atmInterface;
    NotificationService notificationService;

    @BeforeEach
    void init() {
        atmInterface = mock(AtmInterface.class);
        accountDAO = mock(AccountDAO.class);
        notificationService = mock(NotificationService.class);
        atmController = new AtmController(atmInterface, accountDAO, notificationService);
    }

    @Test
    void testWithDrawMoneySuccess() throws Exception {
        String accountNum = "12345678";
        Account account = new Account("Sean", accountNum, "1234", 100.0);
        when(accountDAO.findAccount(accountNum)).thenReturn(account);
        
        atmController.cardInserted("12345678");
        verify(atmInterface).printMsg("Please Enter PIN");
        verify(accountDAO).findAccount(accountNum);
        
        atmController.checkPINCode("1234");
        verify(atmInterface).printMsg("Please Enter Amount");

        double amount = 100d;
        atmController.withDrawAmount(amount);
        verify(atmInterface).dispenseMoney(amount);
        verify(atmInterface).printMsg("Please take your money");
        verify(atmInterface).printReceipt("Receipt for " + amount + " euro");
        assertThat(account.getBalance(), is(0.0));
    }

    @Test
    void testWithDrawMoneyAccountNotFound() throws Exception {
        String accountNum = "12345678";

        when(accountDAO.findAccount(accountNum)).thenReturn(null);
        Throwable e = assertThrows(ATMAccountException.class, () -> {
            atmController.cardInserted("12345678");
        });
        assertThat(e.getMessage(), is("Unrecognized: " + accountNum));
        verify(accountDAO).findAccount(accountNum);
    }
    
    @Test
    void testWithDrawMoneyIncorrectPINOnce() throws Exception {
        String accountNum = "12345678";
        Account account = new Account("Sean", accountNum, "1235", 100.0);
        when(accountDAO.findAccount(accountNum)).thenReturn(account);
        
        atmController.cardInserted("12345678");
        verify(atmInterface).printMsg("Please Enter PIN");
        verify(accountDAO).findAccount(accountNum);
        
        atmController.checkPINCode("1234");
        verify(notificationService).securityAlert(accountNum);
        verify(atmInterface).printMsg("Invalid PIN- Please Re-Enter");
        
        atmController.checkPINCode("1235");
        verify(atmInterface).printMsg("Please Enter Amount");

        double amount = 100d;
        atmController.withDrawAmount(amount);
        verify(atmInterface).dispenseMoney(amount);
        verify(atmInterface).printMsg("Please take your money");
        verify(atmInterface).printReceipt("Receipt for " + amount + " euro");
        assertThat(account.getBalance(), is(0.0));
    }
    
    @Test
    void testWithDrawMoneyIncorrectPINTwice() throws Exception {
        String accountNum = "12345678";
        Account account = new Account("Sean", accountNum, "1235", 100.0);
        when(accountDAO.findAccount(accountNum)).thenReturn(account);
        
        atmController.cardInserted("12345678");
        verify(atmInterface).printMsg("Please Enter PIN");
        verify(accountDAO).findAccount(accountNum);
        
        atmController.checkPINCode("1234");
        atmController.checkPINCode("1234");
        verify(notificationService, new Times(2)).securityAlert(accountNum);
        verify(atmInterface, new Times(2)).printMsg("Invalid PIN- Please Re-Enter");
        
        atmController.checkPINCode("1235");
        verify(atmInterface).printMsg("Please Enter Amount");

        double amount = 100d;
        atmController.withDrawAmount(amount);
        verify(atmInterface).dispenseMoney(amount);
        verify(atmInterface).printMsg("Please take your money");
        verify(atmInterface).printReceipt("Receipt for " + amount + " euro");
        assertThat(account.getBalance(), is(0.0));
    }
    
    
    @Test
    void testWithDrawMoneyIncorrectPINThree() throws Exception {
        String accountNum = "12345678";
        Account account = new Account("Sean", accountNum, "1235", 100.0);
        when(accountDAO.findAccount(accountNum)).thenReturn(account);
        
        atmController.cardInserted("12345678");
        verify(atmInterface).printMsg("Please Enter PIN");
        verify(accountDAO).findAccount(accountNum);
        
        atmController.checkPINCode("1234");
        atmController.checkPINCode("1234");
        Throwable t = assertThrows(ATMPINLimitException.class, ()->{atmController.checkPINCode("1234");});
        assertThat(t.getMessage(), is("Maximum incorrect PIN limit"));
        verify(notificationService, new Times(2)).securityAlert(accountNum);
        verify(atmInterface, new Times(2)).printMsg("Invalid PIN- Please Re-Enter");
    }
    
    @Test
    void testWithDrawMoneyInsufficientFunds() throws Exception {
        String accountNum = "12345678";
        Account account = new Account("Sean", accountNum, "1234", 100.0);
        when(accountDAO.findAccount(accountNum)).thenReturn(account);
        
        atmController.cardInserted("12345678");
        verify(atmInterface).printMsg("Please Enter PIN");
        verify(accountDAO).findAccount(accountNum);
        
        atmController.checkPINCode("1234");
        verify(atmInterface).printMsg("Please Enter Amount");

        double amount = 120d;
        Throwable t = assertThrows(ATMInsufficientFundsException.class,()->{ atmController.withDrawAmount(amount);});
        assertThat(t.getMessage(), is("Insufficient Funds: "+amount));
    }

}
