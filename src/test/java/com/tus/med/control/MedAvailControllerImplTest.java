package com.tus.med.control;

import java.sql.SQLException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.tus.med.control.MedAvailControllerImpl;
import com.tus.med.dao.MedAvailDAO;
import com.tus.med.entities.Customer;
import com.tus.med.entities.Prescription;
import com.tus.med.entities.Product;
import com.tus.med.exception.MedAvailCustomerException;
import com.tus.med.exception.MedAvailOutOfStockException;
import com.tus.med.exception.MedAvailDAOException;
import com.tus.med.exception.MedAvailPrescriptionException;
import com.tus.med.exception.MedAvailException;
import com.tus.med.payment.CreditCardStrategy;
import com.tus.med.payment.PaymentStrategy;
import com.tus.med.payment.PaypalStrategy;
import com.tus.med.services.CreditCardHandler;
import com.tus.med.services.Dispenser;
import com.tus.med.services.PayPalHandler;
import com.tus.med.services.PaymentHandler;
import com.tus.med.services.PaymentHandlerFactory;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.anyString;
import static org.junit.Assert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.anyDouble;
import static org.mockito.Mockito.anyLong;

import static org.mockito.Mockito.eq;
import org.mockito.internal.verification.Times;

import static org.hamcrest.Matchers.*;

public class MedAvailControllerImplTest {

    MedAvailControllerImpl medAvailController;
    Dispenser dispenser;
    MedAvailDAO medAvailDAO;
    PaymentHandlerFactory paymentHandlerFactory;

    @BeforeEach
    public void setUp() {
        dispenser = mock(Dispenser.class);
        medAvailDAO = mock(MedAvailDAO.class);
        paymentHandlerFactory = mock(PaymentHandlerFactory.class);
        medAvailController = new MedAvailControllerImpl(medAvailDAO, dispenser,
                paymentHandlerFactory);
    }

//Test 1
    @Test
    public void testCustomerNotFoundException()
            throws MedAvailException, SQLException {
        long CUSTOMER_ID = 123456789;
        long prescriptionId = 123;

        when(medAvailDAO.getCustomerForId(CUSTOMER_ID)).thenReturn(null);
        Throwable t = assertThrows(MedAvailCustomerException.class, () -> {
            medAvailController.processPrescription(CUSTOMER_ID, prescriptionId);
        });
        assertThat(t.getMessage(), is("Unknown Customer: " + CUSTOMER_ID));

        verify(medAvailDAO).getCustomerForId(anyLong());
        verify(medAvailDAO).getPrescriptionForId(anyLong());
        verify(paymentHandlerFactory, new Times(0))
                .getPaymentHandler(anyString());
        verify(dispenser, new Times(0)).dispense(anyLong());
    }

//Test 2
    @Test
    public void testPrescriptionNotFoundException()
            throws MedAvailException, SQLException {
        long customerId = 123456789;
        long prescriptionId = 123;
        Customer customer = new Customer(customerId, "John Doe",
                "Someplace nice", "johndoe@gmail.com", "CreditCard", null);

        when(medAvailDAO.getCustomerForId(customerId)).thenReturn(customer);
        Throwable t = assertThrows(MedAvailPrescriptionException.class, () -> {
            medAvailController.processPrescription(customerId, prescriptionId);
        });
        assertThat(t.getMessage(),
                is("Prescription not found: " + prescriptionId));

        verify(medAvailDAO).getCustomerForId(anyLong());
        verify(medAvailDAO).getPrescriptionForId(anyLong());
        verify(paymentHandlerFactory, new Times(0))
                .getPaymentHandler(anyString());
        verify(dispenser, new Times(0)).dispense(anyLong());
    }

//Test 3
    @Test
    public void testOneItemOnPrescriptionSuccess()
            throws MedAvailException, SQLException {
        long customerId = 123456789;
        long prescriptionId = 123;
        String cName = "John Doe", ccNum = "123456789", ccv = "123",
                expireDate = "02/22";
        String paymentType = "CreditCard";
        PaymentStrategy paymentStrat = new CreditCardStrategy(cName, ccNum, ccv,
                expireDate);
        // init customer
        Customer customer = new Customer(customerId, "John Doe",
                "Someplace nice", "johndoe@gmail.com", paymentType,
                paymentStrat);

        // init prescription
        double cost = 20d;
        long prodCode = 1234;
        Product product = new Product(prodCode, cost);
        Prescription prescription = new Prescription();
        prescription.addPrescriptionItem(product);

        when(medAvailDAO.getCustomerForId(customerId)).thenReturn(customer);
        when(medAvailDAO.getPrescriptionForId(prescriptionId))
                .thenReturn(prescription);

        CreditCardHandler paymentHandler = mock(CreditCardHandler.class);

        when(paymentHandlerFactory.getPaymentHandler(eq(paymentType)))
                .thenReturn(paymentHandler);

        when(medAvailDAO.checkProductInStock(eq(prodCode))).thenReturn(true);

        // call method
        medAvailController.processPrescription(customerId, prescriptionId);

        verify(medAvailDAO).getCustomerForId(customerId);
        verify(medAvailDAO).getPrescriptionForId(prescriptionId);
        verify(medAvailDAO).checkProductInStock(prodCode);

        verify(paymentHandlerFactory).getPaymentHandler(eq(paymentType));
        verify(dispenser).dispense(anyLong());

        verify(paymentHandler).pay(prescriptionId, ccNum, cName, ccv,
                expireDate, cost);
    }

//Test 4 
    @Test
    public void testMedAvailDAOExceptionCustomer()
            throws MedAvailException, SQLException {
        long CUSTOMER_ID = 123456789;
        long prescriptionId = 123;

        when(medAvailDAO.getCustomerForId(CUSTOMER_ID))
                .thenThrow(new SQLException());
        Throwable t = assertThrows(MedAvailDAOException.class, () -> {
            medAvailController.processPrescription(CUSTOMER_ID, prescriptionId);
        });
        assertThat(t.getMessage(),
                is("Error in connection to MedAvail database"));

        verify(medAvailDAO).getCustomerForId(anyLong());
        verify(medAvailDAO, new Times(0)).getPrescriptionForId(anyLong());
        verify(paymentHandlerFactory, new Times(0))
                .getPaymentHandler(anyString());
        verify(dispenser, new Times(0)).dispense(anyLong());
    }

// Test5
    @Test
    public void testMedAvailDAOExceptionPrescription()
            throws MedAvailException, SQLException {
        long customerId = 123456789;
        long prescriptionId = 123;
        Customer customer = new Customer(customerId, "John Doe",
                "Someplace nice", "johndoe@gmail.com", "CreditCard", null);

        when(medAvailDAO.getCustomerForId(customerId)).thenReturn(customer);
        when(medAvailDAO.getPrescriptionForId(prescriptionId))
                .thenThrow(new SQLException());
        Throwable t = assertThrows(MedAvailDAOException.class, () -> {
            medAvailController.processPrescription(customerId, prescriptionId);
        });
        assertThat(t.getMessage(),
                is("Error in connection to MedAvail database"));

        verify(medAvailDAO).getCustomerForId(anyLong());
        verify(medAvailDAO).getPrescriptionForId(anyLong());
        verify(paymentHandlerFactory, new Times(0))
                .getPaymentHandler(anyString());
        verify(dispenser, new Times(0)).dispense(anyLong());
    }

//Test 6	
    @Test
    public void testOneItemOnPrescriptionCheckStockException()
            throws MedAvailException, SQLException {
        long customerId = 123456789;
        long prescriptionId = 123;
        Customer customer = new Customer(customerId, "John Doe",
                "Someplace nice", "johndoe@gmail.com", "CreditCard", null);

        double cost = 20d;
        long prodCode = 1234;
        Product product = new Product(prodCode, cost);
        Prescription prescription = new Prescription();
        prescription.addPrescriptionItem(product);

        when(medAvailDAO.getCustomerForId(customerId)).thenReturn(customer);
        when(medAvailDAO.getPrescriptionForId(prescriptionId))
                .thenReturn(prescription);
        when(medAvailDAO.checkProductInStock(prodCode))
                .thenThrow(new SQLException());
        Throwable t = assertThrows(MedAvailDAOException.class, () -> {
            medAvailController.processPrescription(customerId, prescriptionId);
        });
        assertThat(t.getMessage(),
                is("Error in connection to MedAvail database"));

        verify(medAvailDAO).getCustomerForId(anyLong());
        verify(medAvailDAO).getPrescriptionForId(anyLong());
        verify(medAvailDAO).checkProductInStock(prodCode);
        verify(paymentHandlerFactory, new Times(0))
                .getPaymentHandler(anyString());
        verify(dispenser, new Times(0)).dispense(anyLong());
    }

// Test 7
    @Test
    public void testOutOfStockDAOException()
            throws MedAvailException, SQLException {
        long customerId = 123456789;
        long prescriptionId = 123;
        Customer customer = new Customer(customerId, "John Doe",
                "Someplace nice", "johndoe@gmail.com", "CreditCard", null);

        double cost = 20d;
        long prodCode = 1234;
        Product product = new Product(prodCode, cost);
        Prescription prescription = new Prescription();
        prescription.addPrescriptionItem(product);

        when(medAvailDAO.getCustomerForId(customerId)).thenReturn(customer);
        when(medAvailDAO.getPrescriptionForId(prescriptionId))
                .thenReturn(prescription);
        when(medAvailDAO.checkProductInStock(prodCode)).thenReturn(false);
        Throwable t = assertThrows(MedAvailOutOfStockException.class, () -> {
            medAvailController.processPrescription(customerId, prescriptionId);
        });
        assertThat(t.getMessage(), is("Product Out of Stock " + prodCode));

        verify(medAvailDAO).getCustomerForId(anyLong());
        verify(medAvailDAO).getPrescriptionForId(anyLong());
        verify(medAvailDAO).checkProductInStock(prodCode);
        verify(paymentHandlerFactory, new Times(0))
                .getPaymentHandler(anyString());
        verify(dispenser, new Times(0)).dispense(anyLong());
    }

//Test8
    @Test
    public void testTwoItemsOnPrescriptionSuccess()
            throws MedAvailException, SQLException {
        long customerId = 123456789;
        long prescriptionId = 123;
        String cName = "John Doe", ccNum = "123456789", ccv = "123",
                expireDate = "02/22";
        String paymentType = "CreditCard";
        PaymentStrategy paymentStrat = new CreditCardStrategy(cName, ccNum, ccv,
                expireDate);
        // init customer
        Customer customer = new Customer(customerId, "John Doe",
                "Someplace nice", "johndoe@gmail.com", paymentType,
                paymentStrat);

        // init prescription
        double cost1 = 20d, cost2 = 30d;
        long prodCode1 = 1234, prodCode2 = 2345;
        Product product1 = new Product(prodCode1, cost1);
        Product product2 = new Product(prodCode2, cost2);
        Prescription prescription = new Prescription();
        prescription.addPrescriptionItem(product1);
        prescription.addPrescriptionItem(product2);

        when(medAvailDAO.getCustomerForId(customerId)).thenReturn(customer);
        when(medAvailDAO.getPrescriptionForId(prescriptionId))
                .thenReturn(prescription);

        CreditCardHandler paymentHandler = mock(CreditCardHandler.class);

        when(paymentHandlerFactory.getPaymentHandler(eq(paymentType)))
                .thenReturn(paymentHandler);

        when(medAvailDAO.checkProductInStock(anyLong())).thenReturn(true);

        // call method
        medAvailController.processPrescription(customerId, prescriptionId);

        verify(medAvailDAO).getCustomerForId(customerId);
        verify(medAvailDAO).getPrescriptionForId(prescriptionId);
        verify(medAvailDAO, new Times(2)).checkProductInStock(anyLong());

        verify(paymentHandlerFactory).getPaymentHandler(eq(paymentType));
        verify(dispenser, new Times(2)).dispense(anyLong());

        verify(paymentHandler).pay(prescriptionId, ccNum, cName, ccv,
                expireDate, cost1 + cost2);
    }

// Test9
    @Test
    public void testOneItemOnPrescriptionPayPal()
            throws MedAvailException, SQLException {
        long customerId = 123456789;
        long prescriptionId = 123;
        String paymentType = "PayPal";
        
        // init customer
        String email = "johndoe@gmail.com";
        String password = "password";
        PaymentStrategy paymentStrat = new PaypalStrategy(email, password);
        Customer customer = new Customer(customerId, "John Doe",
                "Someplace nice", email, paymentType,
                paymentStrat);

        // init prescription
        double cost = 20d;
        long prodCode = 1234;
        Product product = new Product(prodCode, cost);
        Prescription prescription = new Prescription();
        prescription.addPrescriptionItem(product);

        when(medAvailDAO.getCustomerForId(customerId)).thenReturn(customer);
        when(medAvailDAO.getPrescriptionForId(prescriptionId))
                .thenReturn(prescription);

        PayPalHandler paymentHandler = mock(PayPalHandler.class);

        when(paymentHandlerFactory.getPaymentHandler(anyString()))
                .thenReturn(paymentHandler);

        when(medAvailDAO.checkProductInStock(eq(prodCode))).thenReturn(true);

        // call method
        medAvailController.processPrescription(customerId, prescriptionId);

        verify(medAvailDAO).getCustomerForId(customerId);
        verify(medAvailDAO).getPrescriptionForId(prescriptionId);
        verify(medAvailDAO).checkProductInStock(prodCode);

        verify(paymentHandlerFactory).getPaymentHandler(eq(paymentType));
        verify(dispenser).dispense(anyLong());

        verify(paymentHandler).pay(prescriptionId, email,password,cost);
    }

}
