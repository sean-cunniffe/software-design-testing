package com.tus.boundaries;

import com.tus.atm.control.Account;

public interface AccountDAO {
	public Account findAccount(String accountNum);
}
