package com.tus.med.control;

import com.tus.med.exception.MedAvailException;

public interface MedAvailController {
	void processPrescription(long customerAccountId, long prescriptionId) throws MedAvailException;
}
