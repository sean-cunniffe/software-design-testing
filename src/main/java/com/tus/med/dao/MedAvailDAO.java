package com.tus.med.dao;

import java.sql.SQLException;

import com.tus.med.entities.Customer;
import com.tus.med.entities.Prescription;

public interface MedAvailDAO  {
	Customer getCustomerForId(long customerAccountId) throws SQLException;
	Prescription getPrescriptionForId(long prescriptionId) throws SQLException;
	boolean checkProductInStock(long productId) throws SQLException;
	
}
