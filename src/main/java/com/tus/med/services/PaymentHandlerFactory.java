package com.tus.med.services;

public interface PaymentHandlerFactory {
	 PaymentHandler getPaymentHandler(String type);
}
