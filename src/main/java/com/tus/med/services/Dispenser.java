package com.tus.med.services;

public interface Dispenser {
	void dispense(long productCode);

}
