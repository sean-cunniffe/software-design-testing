package com.ait.investment;

public class Investment {

    static final int MIN_AMOUNT = 1000;
    static final int MAX_AMOUNT = 10_000;
    static final int MIN_TERM = 3;
    static final int MAX_TERM = 10;
    
    public static double calculateInvestmentValue(int term, int startAmount) {
        double result = startAmount;
        double rate = 1.02;

        if (term < MIN_TERM || term > MAX_TERM)
            throw new IllegalArgumentException(
                    "Term: +" + term + "+, must be between " + MIN_TERM + " and " + MAX_TERM);
        if (startAmount < MIN_AMOUNT || startAmount > MAX_AMOUNT)
            throw new IllegalArgumentException("Start amount: +" + startAmount + "+, must be between "+MIN_AMOUNT+" and "+MAX_AMOUNT);

        for (int i = 0; i < term; i++) {
            if (result <= 2999)
                rate = 1.02;
            else if (result <= 4999)
                rate = 1.05;
            else
                rate = 1.07;
            result = result * rate;
        }
        result = Math.round(result * 100d) / 100d;
        return result;
    }
}
