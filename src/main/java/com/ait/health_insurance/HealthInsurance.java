package com.ait.health_insurance;

public class HealthInsurance {
    
    public double calculatePremium(double bmi, int bloodpressure){
        
        if(bmi < 10 || bmi > 30 ){
            throw new IllegalArgumentException("Expected between 10 and 30, got: "+bmi);
        }
        if(bloodpressure < 80 || bloodpressure > 160)
            throw new IllegalArgumentException("Expected between 80 and 160, got: "+bloodpressure);
        
        return 0;
    }

}
