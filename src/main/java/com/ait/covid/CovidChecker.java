package com.ait.covid;

public class CovidChecker {

    public String checkTempForCovid(double temperature) {
        if (temperature > 50 || temperature < 30) {
            throw new IllegalArgumentException("Temperature: " + temperature + " must be between 30 and 50");
        }

        if (temperature > 37.5) {
            return "TEST FOR COVID";
        } else {
            return "OK";
        }
    }

    public String checkBPForCovid(int[] bloodPressure) {
        final int expectedLength = 5;

        if (bloodPressure.length != expectedLength)
            throw new IllegalArgumentException(
                    "Need " + expectedLength + " samples of BP, got " + bloodPressure.length);

        double average = 0;
        for (int bp : bloodPressure) {
            if (bp < 60 || bp > 90)
                throw new IllegalArgumentException(
                        "Blood Pressure value is out of range\nExpecting 60 <= bp <= 90 but got " + bp);
            average += bp;
        }
        average /= expectedLength;

        return average > 74 ? "TEST FOR COVID" : "OK";

    }

}
