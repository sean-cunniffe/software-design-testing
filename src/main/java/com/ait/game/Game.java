package com.ait.game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class Game {
	static final int SCORE_TO_WIN = 20;
	private PairOfDice pairOfDice;
	private Logger logger = LoggerFactory.getLogger(getClass());
	Player player1;
	Player player2;
	
	
	public Game() {
		this.pairOfDice = new PairOfDice();
	}
	
	public PairOfDice getPairOfDice() {
		return pairOfDice;
	}
	public void setPairOfDice(PairOfDice pairOfDice) {
		this.pairOfDice = pairOfDice;
	}



	public void start() {
		int numDiceRolls = 0;
		// display a welcome message
		logger.info("Welcome to the Dice Roller application\n");
		Scanner sc = new Scanner(System.in);
		String player1Name;
		String player2Name;
		logger.info("Please enter player one name: ");
		player1Name = sc.next();
		logger.info("Please enter player two name: ");
		player2Name = sc.next();
		setPlayer1(new Player(player1Name));
		setPlayer2(new Player(player2Name));
		PairOfDice dice = getPairOfDice();
		logger.info("Roll the dice? (y/n): ");
		String choice = sc.next().toLowerCase();
		while (choice.equals("y")) {
			numDiceRolls++;
			logger.info("");
			logger.info("Round {} : ", numDiceRolls);
			logger.info("Rolling dice for player one ");
			dice.roll();
			logger.info("Die 1 is {}", dice.getValue1());
			logger.info("Die 2 is {}", dice.getValue2());
			getPlayer1().setTotalScore(dice.getSum());
			logger.info("Rolling dice for player two ");
			dice.roll();
			logger.info("Die 1 is {}", dice.getValue1());
			logger.info("Die 2 is {}", dice.getValue2());
			getPlayer2().setTotalScore(dice.getSum());
			logger.info("{}", player1);
			logger.info("{}", player2);
			choice = "n";
			if ((getPlayer1().getTotalScore() >= SCORE_TO_WIN) && (getPlayer2().getTotalScore() >= SCORE_TO_WIN)) {
				logger.info("DRAW");
			} else if (getPlayer1().getTotalScore() >= SCORE_TO_WIN) {
				logger.info("{} wins", getPlayer1().getName());
				// goodbye printed twice
				logger.info("Good bye!");
			} else if (getPlayer2().getTotalScore() >= SCORE_TO_WIN) {
				logger.info("{} wins", getPlayer2().getName());
			} else {
				logger.info("No winner yet");

				logger.info("Roll the dice again? (y/n): ");
				choice = sc.next().toLowerCase();
			}
		}
		logger.info("Good bye!");
		sc.close();
	}

	public Player getPlayer1() {
		return player1;
	}

	public void setPlayer1(Player player1) {
		this.player1 = player1;
	}

	public Player getPlayer2() {
		return player2;
	}

	public void setPlayer2(Player player2) {
		this.player2 = player2;
		String a;
		
	}
	
	

}
