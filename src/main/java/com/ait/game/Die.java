package com.ait.game;

import java.util.Random;

public class Die {
	private int value;
	private static int sides = 6;
	Random random;

	public Die() {
		this.value = 1; // initialize value to 1
		random = new Random();
	}

	// roll the die
	public void roll() {
		value = random.nextInt(sides) + 1; // number from 0 to sides
	}

	public int getValue() {
		return value;
	}
}