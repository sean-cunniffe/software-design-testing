package com.ait.leapyear;

public class JavaUtil {

	public static boolean isLeapYear(int year) {
		boolean isDivisableBy4 = year % 4 == 0;
		boolean isDivisableBy100 = year % 100 == 0;
		boolean isDivisableBy400 = year % 400 == 0;

		return isDivisableBy4 && !(isDivisableBy100 && !isDivisableBy400);
	}

}
