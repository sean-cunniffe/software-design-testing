package com.ait.message;

public interface MailServer {
    void send(String email, String msgContent);

    void send(Client client, String msgContent);
}
