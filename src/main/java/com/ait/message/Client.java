package com.ait.message;

public interface Client {
    Long getId();

    String getEmail();
}
