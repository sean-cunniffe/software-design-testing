package com.ait.message;

public interface TemplateEngine {

    String prepareMessage(Template msgTemplate, Client client);

}
