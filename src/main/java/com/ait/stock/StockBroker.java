package com.ait.stock;

public interface StockBroker {
	void buy(Stock stock, Integer quantity);
	void sell(Stock stock, Integer quantity);
	double getQoute(Stock stock);
}
