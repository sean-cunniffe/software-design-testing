package com.ait.example_assessment;

public class CovidChecker {

    public String checkTempForCovid(double temperature) {
        final double MIN_TEMP = 30D;
        final double MAX_TEMP = 50D;
        if (temperature < MIN_TEMP || temperature > MAX_TEMP)
            throw new IllegalArgumentException("Temperature is out of range: ");
        return temperature <= 37.5 ? "OK" : "TEST FOR COVID";
    }

    public String checkBPForCovid(int[] bloodPressure) {
        final int MIN_BP = 60;
        final int MAX_BP = 90;
        double avg = 0;
        if (bloodPressure.length != 5)
            throw new IllegalArgumentException("Expected 5 blood pressure values but got: " + bloodPressure.length);
        for (int bp : bloodPressure) {
            if (bp < MIN_BP || bp > MAX_BP)
                throw new IllegalArgumentException(
                        "Expected blood pressure value between " + MIN_BP + " and " + MAX_BP + ", but got: " + bp);
            avg += bp;
        }
        // get the average and return ok if less than 74
        return Math.round(avg / 5d) > 74 ? "TEST FOR COVID" : "OK";
    }
}
